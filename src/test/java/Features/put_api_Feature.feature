Feature: Trigger the Put API with required parameters 

@putapi 
Scenario: Trigger the API request with valid requestbody parameters 
	Given Update NAME and JOB in request body 
	When Request with playload 
	Then Verify status code 
	And verify response body parameters 
	
@putapi 
Scenario Outline: Trigger the API request with valid requestbody parameters 
	Given Update "<NAME>" and "<JOB>" in request body 
	When Request with playload 
	Then Verify status code 
	And verify response body parameters 
	
	Examples: 
		|NAME|JOB|
		|ajay|hr|
		|sanket|dev|
