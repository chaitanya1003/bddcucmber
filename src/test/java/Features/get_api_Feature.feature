Feature: Trigger the get API with required parameters 

@getapi 
Scenario: Trigger the API with retrieving the list of users 
	Given API endpoint to get list of users 
	When Send the get request with payload 
	Then Validate response status code 
	And validate response body contains list of users