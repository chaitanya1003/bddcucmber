Feature: Trigger the PATCH API with required parameters 

@patchapi 
Scenario: Trigger the API request with valid requestbody parameters 
	Given Replace NAME and JOB in request body 
	When request with playload 
	Then  authenticate status code 
	And  response body should contain parameters 
	
@patchapi 
Scenario Outline: Test patch api with multiple data set 
	Given Replace "<NAME>" and "<JOB>" in request body 
	When  request with payload 
	Then authenticate status code 
	And response body should contain parameters 
	
	Examples: 
		|NAME|JOB|
		|ck|tester|
		|sk|dev|