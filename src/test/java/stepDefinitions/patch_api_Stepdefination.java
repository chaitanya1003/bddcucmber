package stepDefinitions;

import java.time.LocalDateTime;

import org.testng.Assert;

import common_method.api_trigger;
import environment_repositaries.environment;
import environment_repositaries.request_repo;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class patch_api_Stepdefination {
	String requestBody;
	String endpoint;
	Response response;
	ResponseBody responseBody;

	@Given("Replace NAME and JOB in request body")
	public void replace_name_and_job_in_request_body() {
		requestBody = request_repo.patch_request_body();
		endpoint = environment.patch_endpoint();
		// throw new io.cucumber.java.PendingException();
	}

	@Given("Replace {string} and {string} in request body")
	public void replace_and_in_request_body(String string, String string2) {
		requestBody = "{\r\n" + "    \"name\": \"" + string + "\",\r\n" + "    \"job\": \"" + string2 + "\"\r\n" + "}";
		endpoint = environment.patch_endpoint();
		// throw new io.cucumber.java.PendingException();
	}

	@When("request with payload")
	public void request_with_payload() {
		response = api_trigger.patch_api_trigger(requestBody, endpoint);
		// throw new io.cucumber.java.PendingException();
	}

	@When("request with playload")
	public void request_with_playload() {
		response = api_trigger.patch_api_trigger(requestBody, endpoint);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("authenticate status code")
	public void authenticate_status_code() {
		int statuscode = response.statusCode();
		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");
		// throw new io.cucumber.java.PendingException();
	}

	@Then("response body should contain parameters")
	public void response_body_should_contain_parameters() {
		responseBody = response.getBody();
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		// step 4 parse the RequestBody using json path extract RequestBody parameter

		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// 4.1: Generate expected Date
		LocalDateTime currentDate = LocalDateTime.now();
		String expecteddate = currentDate.toString().substring(0, 11);

		// step 5 validate using TestNG Assertion
		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to name sent in RequestBody");
		Assert.assertEquals(res_job, req_job, "Job in ResposneBody is not equal to job sent in RequestBody");
		Assert.assertEquals(res_updatedAt, expecteddate,
				"createdAt is in ResponseBody is not equal to createdAt in request");

		// throw new io.cucumber.java.PendingException();
	}

}