package stepDefinitions;

import java.time.LocalDateTime;

import org.testng.Assert;

import common_method.api_trigger;
import environment_repositaries.environment;
import environment_repositaries.request_repo;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class put_api_Stepdefination {

	String requestBody;
	String endpoint;
	Response response;
	ResponseBody responseBody;

	@Given("Update NAME and JOB in request body")
	public void update_name_and_job_in_request_body() {
		requestBody = request_repo.put_request_body();
		endpoint = environment.put_endpoint();

		// throw new io.cucumber.java.PendingException();
	}

	@Given("Update {string} and {string} in request body")
	public void update_and_in_request_body(String string, String string2) {
		requestBody = "{\r\n" + "    \"name\": \"" + string + "\",\r\n" + "    \"job\": \"" + string2 + "\"\r\n" + "}";
		endpoint = environment.put_endpoint();
		// throw new io.cucumber.java.PendingException();
	}

	@When("Request with playload")
	public void request_with_playload() {
		response = api_trigger.put_api_trigger(requestBody, endpoint);

		// throw new io.cucumber.java.PendingException();
	}

	@Then("Verify status code")
	public void verify_status_code() {
		int statuscode = response.statusCode();
		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");
		// throw new io.cucumber.java.PendingException();
	}

	@Then("verify response body parameters")
	public void verify_response_body_parameters() {
		responseBody = response.getBody();
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		// step 3 : parse the requestBody using json path extract the rquestBody
		// parameter
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// step4 : Generate expected Date
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// step 5 : validate using TestNG Assertion
		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to name sent in RequestBody");
		Assert.assertEquals(res_job, req_job, "Job in ResposneBody is not equal to job sent in RequestBody");
		Assert.assertEquals(res_updatedAt, expecteddate,
				"createdAt is in ResponseBody is not equal to createdAt in request");

		// throw new io.cucumber.java.PendingException();
	}

}
