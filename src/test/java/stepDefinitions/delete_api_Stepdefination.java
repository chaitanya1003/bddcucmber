package stepDefinitions;

import org.testng.Assert;

import common_method.api_trigger;
import environment_repositaries.environment;
import environment_repositaries.request_repo;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class delete_api_Stepdefination {

	String requestBody;
	String endpoint;
	Response response;
	ResponseBody responseBody;

	@Given("Delete NAME and JOB in request body")
	public void delete_name_and_job_in_request_body() {
		requestBody = request_repo.delete_request_body();
		endpoint = environment.delete_endpoint();
		// Write code here that turns the phrase above into concrete actions
		// throw new io.cucumber.java.PendingException();
	}

	@When("request  payload")
	public void request_payload() {
		response = api_trigger.delete_api_trigger(requestBody, endpoint);
		// Write code here that turns the phrase above into concrete actions
		// throw new io.cucumber.java.PendingException();
	}

	@Then("search status code")
	public void search_status_code() {
		int statuscode = response.statusCode();
		Assert.assertEquals(statuscode, 204, "Correct status code not found even after retrying for 5 times");
		// Write code here that turns the phrase above into concrete actions
		// throw new io.cucumber.java.PendingException();
	}

	@Then("should Validate response body parameters")
	public void should_validate_response_body_parameters() {
		// Write code here that turns the phrase above into concrete actions
		// throw new io.cucumber.java.PendingException();
	}

}
